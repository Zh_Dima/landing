import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Admin } from './admin.component';

export const ROUTER_CONFIG = [
  { path: 'admin', component:Admin, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Admin
  ],
  imports: [
    // Components / Directives/ Pipes
    RouterModule.forChild(ROUTER_CONFIG),
    FormsModule,
    BrowserModule
  ]
})
export  class AdminModule {
  static routes = ROUTER_CONFIG;
}

