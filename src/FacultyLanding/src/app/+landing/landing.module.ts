import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Landing } from './landing.component';

export const ROUTER_CONFIG = [
  { path: 'landing', component: Landing, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Landing
  ],
  imports: [
    // Components / Directives/ Pipes
    RouterModule.forChild(ROUTER_CONFIG),
    FormsModule,
    BrowserModule
  ]
})
export  class LandingModule {
  static routes = ROUTER_CONFIG;
}

